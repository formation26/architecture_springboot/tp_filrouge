package fr.epita.tpfr.infra;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.springframework.stereotype.Repository;

@Repository
public class ContratFilReaderImpl implements ContratFileReader {

	@Override
	public String readContratFile() {
		BufferedReader br;
		 String fileAsString=null;
		try {
			br = new BufferedReader(new FileReader("contrat.txt"));
			String line = br.readLine();
            StringBuilder sb = new StringBuilder();

            while (line != null) {
                sb.append(line).append("\n");
                line = br.readLine();
            }
            fileAsString= sb.toString();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}            
	            
		return fileAsString;
	}

}
