package fr.epita.tpfr.infra;

public interface ContratFileReader {
	
	String readContratFile();

}
