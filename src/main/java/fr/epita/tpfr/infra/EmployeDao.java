package fr.epita.tpfr.infra;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.epita.tpfr.domaine.Employe;


@Repository
public interface EmployeDao extends JpaRepository<Employe, Long>{

	Employe findByNom(String nom);
	
	

}
