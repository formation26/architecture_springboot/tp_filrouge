package fr.epita.tpfr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@ComponentScan(basePackages = {"fr.epita.tp18","com.tpfr.domaine"})
public class App {

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);

	}

}
