package fr.epita.tpfr.application;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import fr.epita.tpfr.domaine.Contrat;
import fr.epita.tpfr.domaine.Employe;
import fr.epita.tpfr.infra.ContratFileReader;
import fr.epita.tpfr.infra.EmployeDao;

@Service
public class ServiceRHImpl implements ServiceRH {

	@Autowired
	EmployeDao employeDao;
	
	@Autowired
	ContratFileReader reader;
	
	@Autowired
	JmsTemplate jsmTemplate;
	
	public void ajouterNouvelArrivant(Employe nouvelArrivant) {
		
		
		employeDao.save(nouvelArrivant);

	}

	@Override
	public Employe trouverEmploye(String nom) {
		// TODO Auto-generated method stub
		return employeDao.findByNom(nom);
	}

	@Override
	public String genererContrat(Employe employe) {
		
         String template=reader.readContratFile();
         
         String s1=template.replace("[NOM]", employe.getNom());
         String s2=s1.replace("[VILLENAISSANCE]", employe.getAdresse().getVille());
         String s3=s2.replace("[ADRESSE]", employe.getAdresse().getNomRue());
         
         Date d=new Date();
         SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
         String dateStr=sdf.format(d);
         
         String s4=s3.replace("[DATE]", dateStr);
         
		return s4;
	}



	@Override
	public void signerContrat(Employe e) {
		Contrat c =new Contrat();
		c.setDateDebut(new Date());
		
		Calendar cal=Calendar.getInstance();
		cal.add(Calendar.MONTH,6);
		
		Date dateFin=cal.getTime();
		
		c.setFinContrat(dateFin);
		
		e.setContrat(c);
		
		employeDao.save(e);
	}



	@Override
	@Transactional
	public void augmenterSalaire(Employe e) {
		employeDao.save(e);
		jsmTemplate.send("Queue_remuneration",new EmployeMessage(e));
		
	}
	
	
	
	
	

}
