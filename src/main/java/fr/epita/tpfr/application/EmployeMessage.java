package fr.epita.tpfr.application;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.springframework.jms.core.MessageCreator;

import fr.epita.tpfr.domaine.Employe;

public class EmployeMessage implements MessageCreator{
	
	Employe employe;
	
	public  EmployeMessage(Employe employe) {
		this.employe=employe;
	}

	@Override
	public Message createMessage(Session session) throws JMSException {
		ObjectMessage message=session.createObjectMessage(employe);
		return message;
	}

}
