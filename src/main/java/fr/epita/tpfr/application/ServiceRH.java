package fr.epita.tpfr.application;

import fr.epita.tpfr.domaine.Employe;

public interface ServiceRH {
	
	
	void ajouterNouvelArrivant(Employe nouvelArrivant);
	
	
	Employe trouverEmploye(String nom);
	
	String genererContrat(Employe employe);
	
	void signerContrat(Employe e );
	
	void augmenterSalaire(Employe e);

}
