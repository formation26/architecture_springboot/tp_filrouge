package fr.epita.tpfr.exposition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.epita.tpfr.application.ServiceRH;
import fr.epita.tpfr.domaine.Employe;

@RestController
@RequestMapping("/serviceRH")
public class ServiceRhController {
	
	@Autowired
	ServiceRH service;
	
	@PostMapping("/employe")
	public void ajouterNouvelArrivant(@RequestBody Employe employe) {
		service.ajouterNouvelArrivant(employe);
	}
	
	
	@GetMapping("/employe/contrat/{name}")
	public String genererContrat(@PathVariable("name") String nom) {
		 Employe employe=service.trouverEmploye(nom);
		 service.signerContrat(employe);
		return service.genererContrat(employe);
	}
	
	@GetMapping("/employe/{name}")
	public Employe trouverEmploye(@PathVariable("name") String nom) {
		 Employe employe=service.trouverEmploye(nom);
		 
		return employe;
	}
	
	@PutMapping("/employe/salaire")
	public void augmenterSalaire(@RequestBody Employe employe) {
		service.augmenterSalaire(employe);
	}
	

}
