Contrat de travail

La société EPITA dont le siège social se situe 14 RUE VOLTAIRE 94270 LE KREMLIN-BICETRE 
d'une part 
et,
Mme/M [NOM] Né(e) le [dateNaissance], à [VILLENAISSANCE] Demeurant à [ADRESSE] d'autre part, il a été convenu ce qui suit,

ARTICLE 1 ENGAGEMENT

La société EPITA engage Mme/M [NOM], à compter du [DATE]. 
